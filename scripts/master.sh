
MASTER_IP="100.0.0.10"
POD_CIDR="10.244.0.0/16"

# Initialize Kubernetes master
# ======== For GFW =================== https://blog.51cto.com/purplegrape/2315451
sudo chmod 666 /var/run/docker.sock
sudo kubeadm config images list |sed -e 's/^/docker pull /g' -e 's#k8s.gcr.io#registry.aliyuncs.com/google_containers#g' |sh -x
sudo docker images |grep google_containers |awk '{print "docker tag ",$1":"$2,$1":"$2}' |sed -e 's#registry.aliyuncs.com/google_containers#k8s.gcr.io#2' |sh -x
sudo docker images |grep google_containers |awk '{print "docker rmi ", $1":"$2}' |sh -x
# ======== For GFW ===================
sudo kubeadm init --apiserver-advertise-address=$MASTER_IP --pod-network-cidr=$POD_CIDR 

#Move kube config file to current user
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config

# Save Configs to shared /Vagrant location  `config.vm.synced_folder ".", "/vagrant", disabled: false`

# For Vagrant re-runs, check if there is existing configs in the location and delete it for saving new configuration.

config_path="/vagrant/configs"

if [ -d $config_path ]; then
   sudo rm -f $config_path/*
else
   sudo mkdir -p /vagrant/configs
fi

sudo cp -i /etc/kubernetes/admin.conf /vagrant/configs/config
sudo kubeadm token create --print-join-command > join.sh
sudo mv join.sh /vagrant/configs/
sudo chmod +x /vagrant/configs/join.sh       

# Install  Calico Network Plugin
curl https://docs.projectcalico.org/manifests/calico.yaml -O
kubectl apply -f calico.yaml

# Also can use flannel Network Plugin
# curl https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml -O
# sed -i '/- --kube-subnet-mgr/a\        - --iface=eth1' kube-flannel.yml
# kubectl apply -f kube-flannel.yml

sudo -i -u vagrant bash << EOF
mkdir -p /home/vagrant/.kube
sudo cp -i /vagrant/configs/config /home/vagrant/.kube/
sudo chown 1000:1000 /home/vagrant/.kube/config
EOF









