KUBERNETES_VERSION="1.20.10-0"
DOCKER_VERSION="19.03.15"

# Update /etc/hosts
echo "100.0.0.10  k8s-master" >> /etc/hosts
echo "100.0.0.11  k8s-worker-01" >> /etc/hosts
echo "100.0.0.12  k8s-worker-02" >> /etc/hosts

# YUM update
sudo yum install -y yum-utils iproute-tc

# Add Docker repo
sudo yum-config-manager \
    --add-repo \
    https://download.docker.com/linux/centos/docker-ce.repo 

# Use Tsinghua Mirror
sudo sed -i 's+download.docker.com+mirrors.tuna.tsinghua.edu.cn/docker-ce+' /etc/yum.repos.d/docker-ce.repo

# Install Docker
sudo yum -y install docker-ce-$DOCKER_VERSION docker-ce-cli-$DOCKER_VERSION containerd.io

sudo systemctl enable docker
sudo systemctl start  docker
sudo systemctl status docker

# Disable SELinux
sudo setenforce 0
sudo sed -i 's/^SELINUX=enforcing$/SELINUX=permissive/' /etc/selinux/config

# Disable CentOS firewall
sudo systemctl disable firewalld
sudo systemctl stop firewalld

# Disable swapping
sudo swapoff -a
sudo sed -i '/ swap / s/^\(.*\)$/#\1/g' /etc/fstab

# Add Kubernetes repo(Tsinghua Mirror)
sudo cat <<EOF > /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://mirrors.tuna.tsinghua.edu.cn/kubernetes/yum/repos/kubernetes-el7-x86_64/
enabled=1
gpgcheck=0
EOF

# Install Kubernetes
sudo yum install -y kubelet-$KUBERNETES_VERSION kubectl-$KUBERNETES_VERSION kubeadm-$KUBERNETES_VERSION
sudo systemctl enable kubelet
sudo systemctl start kubelet
